#!/usr/bin/env python3

import csv
import json
import pandas as pd
import re
import sys

"""
read gaz_names csv and return list that contains:
- dicts of Māori names (with their english particles removed)
- their geometry in GeoJSON format

:params:
name: gaz_names, type: str

:return:
name: gaz_names_maori, type: list
"""
def find_maori_names(gaz_names):
    result = []
    with open(gaz_names, newline='', encoding='utf8') as csvfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            if row["maori_name"] == "Yes":
                name=remove_english_fragment(row["name"])
                if name == '':
                    continue
                else:
                    item = {
                        "name": name
                    }
                    if row["crd_datum"] != None and row["crd_latitude"] != None and row["crd_longitude"] != None:
                        item["geo"] = {
                            "type": "Feature",
                            "geometry": {
                                "type": "Point",
                                "coordinates": [float(row["crd_latitude"]),float(row["crd_longitude"])]
                            },
                            "properties": {
                                "name": name
                            }
                        }
                    result.append(item)
    return result


"""
splits a name into its parts by space
searches for non-māori letters in a word and removes the word if it exists

:params: 
name: ingoa, type: str

:returns:
name: result, type: str
"""
def remove_english_fragment(ingoa):
    result = []
    english_fragments = re.compile(r'[B|C|D|F|J|L|Q|S|V|X|Y|Z|\/]', re.IGNORECASE)
    extras = ['Area', 'Arm', 'Arthur', 'Earth', 'Irrigation', 'Management', 'Marae', 'Marina', 'Marine', 'Mining', 'Mountain', 'Nature', 'Park', 'Peak', 'Pig', 'Point', 'Range', 'Right', 'Trough']
    replace = r'[(|)|"]'
    cleaned = re.sub(replace, "", ingoa)
    ingoa_fragments = cleaned.strip().split()
    for i in ingoa_fragments:
        if i in extras or english_fragments.search(i) or i.startswith("G"):
            continue
        else:
            result.append(i)
    return " ".join(result)
    

def write_geoJSON(n, file_name):
    result = {
        "type": "FeatureCollection",
        "features": [i["geo"] for i in n]
    }
    with open(file_name, 'w', encoding='utf8') as f:
        json.dump(result, f)
        f.close()
    return

    
def write_csv(n, file_name):
    with open(file_name, 'w', newline='', encoding='utf8') as csvfile:
        headers = ['name', 'count', 'diphthong', 'long_vowel', 'latitude', 'longitude']
        writer = csv.DictWriter(csvfile, fieldnames=headers, delimiter=",")
        writer.writeheader()
        for i in n:
            r=check_grammar(i["name"])
            r["latitude"] = i["geo"]["geometry"]["coordinates"][0]
            r["longitude"] = i["geo"]["geometry"]["coordinates"][1]
            writer.writerow(r)
        csvfile.close()
    return


def check_grammar(ingoa):
    result = {}
    result["name"] = ingoa
    ingoa_lower = ingoa.lower()
    long_vowel_list = ['ā','ē','ī','ō','ū']
    all_vowel_list = ['a','e','i','o','u','ā','ē','ī','ō','ū']
    # any combinations of vowels
    diphthong = list(set([x+y for x in all_vowel_list for y in all_vowel_list]))
    if any(d in ingoa_lower for d in diphthong):
        result["diphthong"] = "Y"
    else:
        result["diphthong"] = "N"
    if any(l in ingoa_lower for l in long_vowel_list):
        result["long_vowel"] = "Y"
    else:
        result["long_vowel"] = "N"
    count=0
    for a in all_vowel_list:
        count+=ingoa_lower.count(a)
    result["count"] = count
    return result
    


names = find_maori_names("public/gaz_names.csv")
write_geoJSON(names, "public/gaz_names_maori.json")
write_csv(names, "public/gaz_names_maori.csv")

df = pd.read_csv("public/gaz_names_maori.csv")
sorted_df = df.sort_values(by=["diphthong", "long_vowel", "count"], ascending=[True, True, True])
sorted_df.to_csv("public/gaz_names_maori.csv", index=False)
#print(result) 

