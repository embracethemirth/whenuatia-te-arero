#!/usr/bin/env python3

import csv
import gaz_names_maori
import json
import pandas as pd
import re
import sys

"""
read gaz_names csv and return list that contains:
- dicts of Māori names (with their english particles removed)
- their geometry in GeoJSON format

:params:
name: gaz_names, type: str

:return:
name: gaz_names_maori, type: list
"""
def find_names(all_names):
    result = []
    with open(all_names, newline='', encoding='utf8') as csvfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            result.append(gaz_names_maori.remove_english_fragment(row['\ufeffPlace Name']))
    return result

    
def write_csv(n, file_name):
    with open(file_name, 'w', newline='', encoding='utf8') as csvfile:
        headers = ['name', 'count', 'diphthong', 'long_vowel']
        writer = csv.DictWriter(csvfile, fieldnames=headers, delimiter=",")
        writer.writeheader()
        for i in n:
            writer.writerow(gaz_names_maori.check_grammar(i))
        csvfile.close()
    return
pre="public/nzhistory_1000_names.csv"
post="public/nzhistory_1000_names_sorted.csv"

names = find_names(pre)
write_csv(names, post)
df = pd.read_csv(post)
sorted_df = df.sort_values(by=["diphthong", "long_vowel", "count"], ascending=[True, True, True])
sorted_df.to_csv(post, index=False)
#print(result) 