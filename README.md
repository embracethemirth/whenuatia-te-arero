## Kua tīkina ngā ingoa Māori nō:
[LINZ Gazetteer](https://gazetteer.linz.govt.nz/):
- Raw [gaz_names](https://embracethemirth.gitlab.io/whenuatia-te-arero/gaz_names.csv) file
- Valid GeoJSON [gaz_names_maori](https://embracethemirth.gitlab.io/whenuatia-te-arero/gaz_names.csv) file, Māori names only
- **HEAPS OF DUPLICATES** CSV [gaz_names_maori](https://embracethemirth.gitlab.io/whenuatia-te-arero/gaz_names_maori.csv), Māori names only, vowels are counted, long vowels and diphthongs are mentioned

[NZ History](https://nzhistory.govt.nz/culture/maori-language-week/1000-maori-place-names):
- Raw [nzhistory_1000](https://embracethemirth.gitlab.io/whenuatia-te-arero/nzhistory_1000_names.csv) file
- CSV [nzhistory_1000](https://embracethemirth.gitlab.io/whenuatia-te-arero/nzhistory_1000_names_sorted.csv) file vowels are counted, long vowels and diphthongs are mentioned
